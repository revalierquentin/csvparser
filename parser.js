const csv = require('csv-parser');
const fs = require('fs');
const https = require('https');
const decompress = require("decompress");
nbTransfertSiege = 0;
nbTotal = 0;

async function downloadFile() {
    return new Promise((resolve, reject) => {
        try {
            if (fs.existsSync("./csv/StockEtablissementLiensSuccession_utf8.csv")) {
                console.log("Le fichier existe déjà !");
                resolve(true);
            } else {
                const file = fs.createWriteStream("StockEtablissementLiensSuccession_utf8.zip");
                console.log("Début du téléchargement !");
                const request = https.get("https://files.data.gouv.fr/insee-sirene/StockEtablissementLiensSuccession_utf8.zip", function (response) {
                    response.pipe(file);
                    file.on("finish", () => {
                        file.close();
                        console.log("Fin du téléchargement !");
                        decompress("StockEtablissementLiensSuccession_utf8.zip", "csv")
                            .then((files) => {
                                console.log(files);
                                console.log("Le fichier a bien été dézippé !");
                                resolve(true);
                            })
                            .catch((error) => {
                                console.error(error);
                                reject(error);
                            });
                    });
                });
            }
        } catch (error) {
            console.error(error);
            reject(error);
        }
    });
}

async function getPercentage() {
    return new Promise((resolve, reject) => {
        try {
            fs.createReadStream('csv/StockEtablissementLiensSuccession_utf8.csv')
                .pipe(csv())
                .on('data', (data) => {
                    nbTotal += 1;
                    if (data["transfertSiege"] == "true") {
                        nbTransfertSiege += 1;
                    }
                    console.log(nbTotal / 8150337);
                    //console.log(data["transfertSiege"] == "true");
                })
                .on('end', () => {
                    console.log("OK");
                    console.log(nbTransfertSiege / nbTotal);
                    resolve(nbTransfertSiege / nbTotal);
                });
        }catch(error){
            console.error(error);
            reject(error);
        }
    });
}

module.exports = { getPercentage, downloadFile };