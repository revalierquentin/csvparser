const parser = require('./parser');
const express = require('express');
const app = express();
const port = 3000;

app.get('/', (req, res) => {
    res.send('Bienvenue sur votre Parser ! <br><br> Rendez-vous <a href="http://localhost:' + port + '/tp1">ici</a> pour obtenir le pourcentage souhaité !');
})

app.get('/tp1', async function (req, res) {
    parser.downloadFile().then((response) => {
        if (response == true) {
            parser.getPercentage().then((response) => {
                res.send(200, response);
            })
        }
        else{
            res.send(500, "Une erreur est survenue !")
        }
    })
})

app.listen(port, () => {
    console.log(`Parser app listening on http://localhost:${port}`);
})